﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigHelpers
{
    public static class ConfigHelper
    {
        private static object obj = new object(); // only used for locking
        public static string GetFromConfig(string key_in_app_config)
        {
            lock (obj)
            {
                string value_from_config = ConfigurationManager.AppSettings[key_in_app_config];
                if (String.IsNullOrEmpty(value_from_config))
                    throw new ConfigurationMissing();
                return value_from_config;
            }
            
        }
        public static int DefaultMapSize
        {
            get
            {
                lock (obj)
                {
                    int result;
                    string value_from_config = ConfigurationManager.AppSettings["DefaultMapSize"];
                    if (String.IsNullOrEmpty(value_from_config))
                        throw new ConfigurationMissing();

                    if (Int32.TryParse(value_from_config, out result))
                    {
                        return result;
                    }
                    else
                    {
                        throw new ConfigurationNotParseable();
                    }
                }
            }
        }

        public static bool StartingWithRandomSeed
        {
            get
            {
                lock (obj)
                {
                    bool result;
                    string value_from_config = ConfigurationManager.AppSettings["DefaultMapSize"];
                    if (String.IsNullOrEmpty(value_from_config))
                        throw new ConfigurationMissing();

                    if (Boolean.TryParse(value_from_config, out result))
                    {
                        return result;
                    }
                    else
                    {
                        throw new ConfigurationNotParseable();
                    }
                }
            }
        }
        public static int Seed
        {
            get
            {
                lock (obj)
                {
                    int result;
                    string value_from_config = ConfigurationManager.AppSettings["DefaultMapSize"];
                    if (String.IsNullOrEmpty(value_from_config))
                        throw new ConfigurationMissing();

                    if (Int32.TryParse(value_from_config, out result))
                    {
                        return result;
                    }
                    else
                    {
                        throw new ConfigurationNotParseable();
                    }
                }
            }
        }
    }
}
