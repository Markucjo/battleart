﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConfigHelpers
{
    [Serializable]
    public class ConfigurationMissing : ConfigurationErrorsException
    {
        public ConfigurationMissing()
        { }
        public ConfigurationMissing(string message)
        : base(message)
        { }

        public ConfigurationMissing(string message, Exception innerException)
            : base(message, innerException)
        { }
    }

    [Serializable]
    public class ConfigurationNotParseable : ConfigurationErrorsException
    {
        public ConfigurationNotParseable()
        { }
        public ConfigurationNotParseable(string message)
          : base(message)
        {
        }

        public ConfigurationNotParseable(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}
