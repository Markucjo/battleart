﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle_map
{
    public enum Field_type_enum
    {
        GRASS = 1,
        ROCKS,
    }

    static public class Battle_map_dics
    {
        public static Dictionary<Field_type_enum, int> Type_move_cost = new Dictionary<Field_type_enum, int>() {
            { Field_type_enum.GRASS, 100 },
            { Field_type_enum.ROCKS, 150 }
        };
    }

}
