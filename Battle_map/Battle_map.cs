﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigHelpers;
using NLog;

namespace Battle_map
{
    public class Battle_map
    {
        private List<Battle_map_field>[,] battle_map_fields;
        
        static readonly private Logger log = LogManager.GetCurrentClassLogger();
        Battle_map(List<Battle_map_field>[,] input)
        {
            log.Trace("Generated map from inputted data.");
            battle_map_fields = input;
        }

        Battle_map()
        {
            log.Trace("Generated map with default data.");
            int default_size = ConfigHelpers.ConfigHelper.DefaultMapSize;
            battle_map_fields = new List<Battle_map_field>[default_size, 25];

            for(int i=0;i< default_size; i++)
            {
                for(int j=0;j< default_size;j++)
                {
                    battle_map_fields[i, j].Add(new Battle_map_field(i, j, 5, Field_type_enum.GRASS));
                }
            }
        }
    }

    public class edge
    {

    }
}
