﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle_map
{
    public class Battle_map_object
    {
        private readonly string _name;
        private List<Map_object_type_enum> _types;
        private int _x, _y, _z;

        public int x
        {
            get
            {
                return _x;
            }
            set
            {
                _x = value;
            }
        }

        public int y
        {
            get
            {
                return _y;
            }
            set
            {
                _y = value;
            }
        }

        public int z
        {
            get
            {
                return _z;
            }
            set
            {
                _z = value;
            }
        }
        public Battle_map_object()
        {

        }

        public bool IsOfType(Map_object_type_enum type)
        {
            return _types.Contains(type);
        }


    }
}
