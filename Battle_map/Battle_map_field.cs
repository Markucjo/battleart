﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Battle_map
{
    public class Battle_map_field
    {
        private int _x, _y, _z;
        private int _move_cost;
        private Field_type_enum _field_type;
        // private List<Field_effects> _effects_list;

        public Battle_map_field(int px, int py, int pz, Field_type_enum pfield_type)
        {
            x = px;
            y = py;
            z = pz;
            field_type = pfield_type;
        }
        public int x
        {
            set
            {
                _x = value;
            }
            get
            {
                return _x;
            }
        }
        public int y
        {
            set
            {
                _y = value;
            }
            get
            {
                return _y;
            }
        }
        public int z
        {
            set
            {
                _z = value;
            }
            get
            {
                return _z;
            }
        }
        public int move_cost
        {
            set
            {
                _move_cost = value;
            }
            get
            {
                return _move_cost;
            }
        }

        public Field_type_enum field_type
        {
            set
            {
                float old_modifier = _move_cost / Battle_map_dics.Type_move_cost[_field_type] + 1;
                _field_type = value;
                _move_cost = (int)(Battle_map_dics.Type_move_cost[value]* old_modifier);
            }
            get
            {
                return _field_type;
            }
        }

        public int resetField()
        {

            _move_cost = Battle_map_dics.Type_move_cost[_field_type];
            return _move_cost;
        }
    }
}
