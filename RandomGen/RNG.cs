﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConfigHelpers;
using System.Security.Cryptography;
using System.Configuration;

namespace RandomGen
{
    public static class RNG
    {
        static private readonly int _current_seed;
        static private Random _random;
        static private readonly bool _is_manual;
        static RNG()
        {
            Random startup_rng_gen = new Random();
            int seed;

            try { _is_manual = ConfigHelper.StartingWithRandomSeed; }
            catch (ConfigurationMissing) { _is_manual = false; }
            catch (Exception E) { throw E; }
            
            if(_is_manual)
            {
                try { seed = ConfigHelper.Seed; }
                catch (Exception E) { throw E; }
            }
            else
            {
                seed = startup_rng_gen.Next();
            }
            _current_seed = seed;
            _random = new Random(seed);
        }

        public static int NextValue { get { return _random.Next(); } }
        public static int Seed { get { return _current_seed; } }
    }
}
